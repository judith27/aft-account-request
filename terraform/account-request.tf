# Copyright Amazon.com, Inc. or its affiliates. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

module "Demo_Account_2" {
  source = "./modules/aft-account-request"

  control_tower_parameters = {
    AccountEmail = "us_aws-control-tower-accelerator+AFT-Account_custom0015@pwc.com"
    AccountName  = "Demo Account 2 "
    # Syntax for top-level OU
    ManagedOrganizationalUnit = "Sandbox"
    # Syntax for nested OU
    # ManagedOrganizationalUnit = "Sandbox (ou-xfe5-a8hb8ml8)"
    SSOUserEmail     = "justin.guse@pwc.com"
    SSOUserFirstName = "Justin"
    SSOUserLastName  = "Guse"
  }

  account_tags = {
    "Owner"       = "justin.guse@pwc.com"
  }

   change_management_parameters = {
    change_requested_by = "Justin"
    change_reason       = "new demo idle sandbox account 2 for future demos"
  }
  account_customizations_name = "sandbox-demo"
}



module "Demo_Account_2" {
  source = "./modules/aft-account-request"

  control_tower_parameters = {
    AccountEmail = "us_aws-control-tower-accelerator+AFT-Account_custom0016@pwc.com"
    AccountName  = "Demo Account 2 "
    # Syntax for top-level OU
    ManagedOrganizationalUnit = "Sandbox"
    # Syntax for nested OU
    # ManagedOrganizationalUnit = "Sandbox (ou-xfe5-a8hb8ml8)"
    SSOUserEmail     = "justin.guse@pwc.com"
    SSOUserFirstName = "Justin"
    SSOUserLastName  = "Guse"
  }

  account_tags = {
    "Owner"       = "justin.guse@pwc.com"
  }

   change_management_parameters = {
    change_requested_by = "Justin"
    change_reason       = "new demo idle sandbox account 2 for future demos"
  }
  account_customizations_name = "sandbox-demo"
}

